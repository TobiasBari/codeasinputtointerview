import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import SongsService from "@/services/songsService.js";
import ImportService from "@/services/ImportService.js";

// function _createOccasionsById(occasionList) { //NOTE: premature optimization:  Don't think we need this since the amount of data can be looked up in arrays through looping instead.
//   const occasionsById = {};
//   for (let i = 0; i < occasionList.length; i++) {
//     const occ = occasionList[i];
//     occasionsById[occ.id] = occ;
//   }
//   return occasionsById;
// }

function _getImportMetaDataParameters(d) {
  const songList = d.songList;
  const personList = d.personList;
  const occasionTypeList = d.occasionTypeList;
  return {
    songList: songList,
    personList: personList,
    occasionTypeList: occasionTypeList
  };
}

function findById(id, list) {
  if (id === null) return null;
  return list.find(s => s.id == id);
}

function createDictionaryByIdFromList(listWithId){
  const dictionary = {}
  for (let i = 0; i < listWithId.length; i++) {
    const element = listWithId[i];
    dictionary[element.id] = element;
  } 
  return dictionary;
}

function findOrCreateEmpty(id, list){
  let suggested = findById(id, list);
  if (!suggested){
    let emptyValue = findById(0, list);
    if (emptyValue) return emptyValue;
    return { id:0, name:'(empty)', roleBitMask: 0};
  }
  return suggested;
}

function _createOccasionsDataStructures(
  occasionList_WithSuggestions,
  songList,
  personList,
  occasionList_Static
) {
  function __createOccasionForm(oWithSug, staticOccasion) {
    const recordingListForm = [];
    oWithSug.recordingList.forEach(r => {   
      const suggestedSong = findOrCreateEmpty(r.suggestions.songId, songList);

      const recordingDataStructure = {
        id: r.id,
        fileName: r.fileName,
        song: suggestedSong,
        startBar: "",
        endBar: "",
        counterId: null,
        timeDuration: r.timeDuration,
        comment: ""
      };
      if (r.suggestions.studioRecording) {
        recordingDataStructure.startBar = r.suggestions.studioRecording.startBar;
        recordingDataStructure.counterId = r.suggestions.studioRecording.counterId;
      }
      recordingListForm.push(recordingDataStructure);
    });

    const suggestions = oWithSug.suggestions;
    const suggestedSong = findOrCreateEmpty(suggestions.songId, songList);
    const suggestedPianist = findOrCreateEmpty(suggestions.pianistId, personList);
    const suggestedCoach = findOrCreateEmpty(suggestions.coachId, personList);

    const occasionDataStructure = {
      id: oWithSug.id,
      name: suggestions.name,
      date: suggestions.date,
      song: suggestedSong,
      pianist: suggestedPianist,
      coach: suggestedCoach,
      occasionTypeId: suggestions.occasionTypeId,
      location: "",
      comment: "",
      recordingList: recordingListForm
    };
    return occasionDataStructure;
  }

  const staticOccasionById = createDictionaryByIdFromList(occasionList_Static);

  const occasionForm = { clean: [], dirty: [] };
  occasionList_WithSuggestions.forEach(o => {
    let staticOccasion = staticOccasionById[o.id];
    let dirty = __createOccasionForm(o, staticOccasion);
    let clean = JSON.parse(JSON.stringify(dirty));
    occasionForm.dirty.push(dirty);
    occasionForm.clean.push(clean);

    //NOTE: Can't get below to work (i.e. dynamically adding the dirtyOccasion to the staticOccasion as an easy way to look it up in the component)
    // staticOccasion.dirtyOccasion = dirty;
  });
  return occasionForm;
}

export default new Vuex.Store({
  state: {
    import: {
      step: 1,
      occasionStaticList: [],
      occasionListForm: {
        dirty: [],
        clean: []
      },
      detailsLoaded: false
    },
    main: {
      songListByGenre: {},
      songList: [],
      personList: [],
      occasionTypeList: []
    }
  },
  mutations: {
    SET_IMPORT_START(state){
      state.import.step = 1;
    },
    SET_OCCASIONS_STATIC_INFO(state, occasionStaticList) {
      state.import.occasionStaticList = occasionStaticList;
    },
    SET_IMPORT_STEP(state, step) {
      state.import.step = step;
    },
    SET_IMPORT_METADATA(state, p) {
      state.main.songList = p.songList;
      state.main.personList = p.personList;
      state.main.occasionTypeList = p.occasionTypeList;
    },
    SET_OCCASIONS_FORM(state, occasionListForm) {
      state.import.occasionListForm = occasionListForm;
      state.import.detailsLoaded = true;
    }
  },
  actions: {
    fetchImportInformation({ commit }) {
      commit("SET_IMPORT_START");

      ImportService.getImportOverviewInfo()
        .then(response => {
          const occasionList_Static = response.data;
          console.log(occasionList_Static);

          commit("SET_OCCASIONS_STATIC_INFO", occasionList_Static);

          ImportService.getImportDetailsInfo()
            .then(response => {
              const data = response.data;

              const metaData = _getImportMetaDataParameters(data);
              commit("SET_IMPORT_METADATA", metaData);

              const parameters = _createOccasionsDataStructures(
                data.occasionList,
                metaData.songList,
                metaData.personList,
                occasionList_Static
              );
              commit("SET_OCCASIONS_FORM", parameters);
            })
            .catch(error => {
              console.log(error.response);
            });
        })
        .catch(error => {
          console.log(error.response);
        });
    },

    setImportStep2WithSuggestions({ commit }){
      commit("SET_IMPORT_STEP", 2);
    }, 
    setImportStep3Done({ commit }){
      commit("SET_IMPORT_STEP", 3);
    }, 
    fetchSongList({ commit }) {
      const songListByGenre = {};

      SongsService.getSongListContainer()
        .then(response => {
          const container = response.data;
          const genreHash = container["genreHash"];
          const composerHash = container["composerHash"];
          const lyricsAuthorHash = container["lyricsAuthorHash"];
          const songRawList = container["songList"];

          songRawList.forEach(element => {
            const song = {
              id: element.id,
              name: element.name,
              genre: genreHash[element.genreId],
              composer: composerHash[element.composerId],
              lyricsAuthor: lyricsAuthorHash[element.lyricsAuthorId]
            };
            if (!(song.genre in songListByGenre)) {
              songListByGenre[song.genre] = [];
            }
            songListByGenre[song.genre].push(song);
          });
          commit("SET_SONGLIST", songListByGenre);
        })
        .catch(error => {
          console.log(error.response);
        });
    }
  },
  getters: {
    recordingCount: state => dirtyOccasion => {
      if (!state || !dirtyOccasion) return 0;
      return dirtyOccasion.recordingList
        ? dirtyOccasion.recordingList.length
        : 0;
    },
    // occasionsSlimById: state => {
    //   if (state.occasionsSlimList.length) return {};
    //   const occById = {};
    //   for (let i = 0; i < state.occasionsSlimList.length; i++) {
    //     const occ = state.occasionsSlimList[i];
    //     occById[occ.id] = occ;
    //   }
    //   return occById;
    // },
    genreList: state => {
      if (state) return []; //Object.keys(state.songListByGenre);
    },
    songListByRowIx: (state, getters) => ix => {
      //TODO: This is too view dependent to reside in the store. It's not elegant.
      const row = [];
      getters.genreList.forEach(genre =>
        row.push(state.songListByGenre[genre][ix])
      );
      return row;
    },
    maxRowsArray: (state, getters) => {
      //TODO: also too view dependent. It's also feels like an overkill way to find render the table like this...
      let maxRowNbr = 0;
      getters.genreList.forEach(genre => {
        const rowNbrs = state.songListByGenre[genre].length;
        if (rowNbrs > maxRowNbr) {
          maxRowNbr = rowNbrs;
        }
      });
      const indexRow = [];
      for (let i = 0; i < maxRowNbr; i++) {
        indexRow.push(i);
      }

      return indexRow;
    }
  }
});
