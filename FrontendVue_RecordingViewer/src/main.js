import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueSelect from "vue-cool-select";
import constants from "./plugins/constants.js";

Vue.config.productionTip = false;

const theme = "bootstrap";

Vue.use(constants);

Vue.use(VueSelect, {
  theme: theme // "bootstrap" or 'material-design'
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
