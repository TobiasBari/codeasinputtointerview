   /* NOTE: 
      This submit method is trying to minimizing the amount of data that is sent to the server 
      Since there is a lot of suggestions being parsed on the server from the folder and file names on the server, 
      it is necessary to send this information once again to the server since it already exists there. 
      Instead we simply don't add the data to the json payload in which case, the server code will know to use the suggested value (or empty if not a suggestion).

      COMMENT: 
      This optimisation technique is done since the import can contain quite a lot of data to send to the server. 
      It might be a bit of premature optimization since the time difference (compared to mp3 streaming) might not be that big actually. 
      But the dirty/clean data structure gives this opportunity to do this in an easy way and it is a good design concept as such.       
      */

export default function ImportPayLoadFactory() {
  this.createPayLoad = function(occasionListForm) {

    function _cloneOccasionFrom(dirtyOcc, cleanOcc){
      const o = { id : dirtyOcc.id }
    
      //TODO: all these if-statements can be minimized and simplified using Javascript property indexers instead. This will also minimize maintenance. But this will do for now.
      if (dirtyOcc.date !== cleanOcc.date) o.date = dirtyOcc.date;
      if (dirtyOcc.song.id !== cleanOcc.song.id) o.songId = dirtyOcc.song.id;
      if (dirtyOcc.pianist.id !== cleanOcc.pianist.id) o.pianistId = dirtyOcc.pianist.id;
      if (dirtyOcc.coach.id !== cleanOcc.coach.id) o.coachId = dirtyOcc.coach.id;
      if (dirtyOcc.occasionTypeId !== cleanOcc.occasionTypeId) o.occasionTypeId = dirtyOcc.occasionTypeId;
      if (dirtyOcc.name !== cleanOcc.name) o.name = dirtyOcc.name;
      if (dirtyOcc.location !== cleanOcc.location) o.location = dirtyOcc.location;
      if (dirtyOcc.comment !== cleanOcc.comment) o.comment = dirtyOcc.comment;
  
      return o; 
    }
  
    function _cloneRecordingList(dirtyOcc, cleanOcc){
      const list = [];
    
      for (let i = 0; i < dirtyOcc.recordingList.length; i++) {        
        let dirtyRec = dirtyOcc.recordingList[i];
        let cleanRec  = cleanOcc.recordingList[i];
      
        let r = { id : dirtyRec.id };
        if (dirtyRec.song.id !== cleanRec.song.id) r.songId = dirtyRec.song.id;
        if (dirtyRec.startBar != cleanRec.startBar) r.startBar = dirtyRec.startBar;
        if (dirtyRec.endBar != cleanRec.endBar) r.endBar = dirtyRec.endBar;
        if (dirtyRec.comment != cleanRec.comment) r.comment = dirtyRec.comment;          
        list.push(r);
      }
      return list;
    }

    const payLoad = [];
    var dirtyOccasions = occasionListForm.dirty;
    var cleanOccasions = occasionListForm.clean;
  
    for (let i = 0; i < dirtyOccasions.length; i++) {        
      const dirtyOcc = dirtyOccasions[i];
      const cleanOcc  = cleanOccasions[i];
      
      const occasion = _cloneOccasionFrom(dirtyOcc, cleanOcc); 
      occasion.recordingList =  _cloneRecordingList(dirtyOcc, cleanOcc);;
      payLoad.push(occasion);
    }
    return payLoad;
  }
}
