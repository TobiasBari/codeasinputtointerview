export default {
  onChangeOccasionType(occasionId){
    this.dirtyOccasion.occasionTypeId = occasionId;
  },
  isCurrentOccasionTypeChecked(occasionId){
      return occasionId === this.dirtyOccasion.occasionTypeId;
  },
  getCheckboxId(occasionTypeId) {
    return "checkboxType_" + occasionTypeId;
  },
  toggleComment() {
    this.showCommentTextarea = !this.showCommentTextarea;
  },
  findbyId(objectWithId, list) {
    if (!objectWithId) return null;
    const found = list.find(s => s.id == objectWithId.id);
    if (!found) return null;
    return found;
  },
  personName(personId) {
    const person = this.findPerson(personId);
    const personName = person ? person.name : "";
    return personName;
  },
  isSelected(typeidInCheckBox, occasionType) {
    if (typeidInCheckBox === occasionType) return "selected";
    else return "";
  },

  occasionTypeListWithSelected(occasionTypeId) {
    let result = [];
    const typeList = this.occasionTypeList;
    if (!typeList) return result;
    for (let i = 0; i < typeList.length; i++) {
      let o = typeList[i];
      result.push({
        id: o.id,
        name: o.name,
        selected: occasionTypeId === o.id ? "selected" : ""
      });
    }
    return result;
  },
  getOccasionTypeCleanCompare(occasionTypeRadioId){
    if (this.dirtyOccasion.occasionTypeId === this.cleanOccasion.occasionTypeId && occasionTypeRadioId === this.cleanOccasion.occasionTypeId) {
      return this.editTextUnderline;    
    }
    if (occasionTypeRadioId == this.dirtyOccasion.occasionTypeId) {
      return this.suggestionTextUnderline;      
    }
    return "";
  }, 
  getCommentCleanCompare(){
    if (this.dirtyOccasion.comment !== this.cleanOccasion.comment ) return this.editTextUnderline;    
    return {}
  },
  getBorderStyleCleanCompareById(dirtyObject, cleanObject){
    if (!dirtyObject || !cleanObject) return  {};   
    if (dirtyObject.id === 0 && cleanObject.id === 0) return {};
    if (dirtyObject.id !== cleanObject.id) return this.editedBorderStyle;;
    return this.suggestedBorderStyle;
  },
  getBorderStyleCleanCompare(dirtyValue, cleanValue){
    if (!this.dirtyOccasion || !this.cleanOccasion) return  {};   
    if (dirtyValue !== cleanValue) return this.editedBorderStyle;
    return this.suggestedBorderStyle;
  },
  getBorderStyle(dirtyValue){
    if (!this.dirtyOccasion) return  {};   
    if (dirtyValue !== '') return this.editedBorderStyle;
    return {};
  }
}