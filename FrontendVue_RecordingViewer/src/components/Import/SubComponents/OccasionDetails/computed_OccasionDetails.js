export default {
  getCoach() {
    return;
  },
  commentSectionShouldBeVisible() {
    return this.showCommentTextarea || this.dirtyOccasion.comment.length > 0;
  },
  getOccasionRadioButtonName() {
    return "occasionRadioButtonName_" + this.dirtyOccasion.id;
  },
  occasionTypeListWithSelected_NEW() {
    let result = [];
    const typeList = this.occasionTypeList;
    if (!typeList) return result;
    for (let i = 0; i < typeList.length; i++) {
      let o = typeList[i];
      result.push({
        id: o.id,
        name: o.name,
        checked: this.dirtyOccasion.occasionTypeId === o.id ? true : false
      });
    }
    return result;
  },
  isStudioRecording() {
    return this.dirtyOccasion.occasionTypeId === 2;
  },
  occasionTypeList() {
    return this.$store.state.main.occasionTypeList;
  },
  suggestionTextUnderline() {
    return { "text-decoration": "underline", "text-decoration-color": "blue" };
  },
  editTextUnderline() {
    return { "text-decoration": "underline", "text-decoration-color": "green" };
  },
  editedBorderStyle() {
    return { border: "1px solid green" };
  },
  suggestedBorderStyle() {
    return { border: "1px solid blue" };
  },
  pianistList() {
    return this.personList.filter(p => this.$isPianist(p.roleBitMask));
  },
  coachList() {
    return this.personList.filter(p => this.$isCoach(p.roleBitMask));
  },
  nbrOfRecordings() {
    const nbrOfRecordings = this.$store.getters.recordingCount(
      this.dirtyOccasion
    );
    return nbrOfRecordings;
  },
  pianistWrapperFucker() {
    let temp = { item: this.dirtyOccasion.pianist };
    return temp;
  }
  //...mapGetters(["recordingCount"])
};
