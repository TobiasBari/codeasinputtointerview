export default {
  toggleComment() {
    this.showCommentTextarea = !this.showCommentTextarea;
  },
  getBorderStyleCleanCompare(dirtyValue, cleanValue){
    if (!this.dirtyRecording || !this.cleanRecording) return  {};   
    if (!dirtyValue && !cleanValue) return  {};   
    if (parseInt(dirtyValue) !== cleanValue) return this.editedBorderStyle;
    return this.suggestedBorderStyle;
  },
  getBorderStyleCleanCompareById(dirtyObject, cleanObject){
    if (!dirtyObject || !cleanObject) return  {};   
    if (dirtyObject.id === 0 && cleanObject.id === 0) return {};
    if (dirtyObject.id !== cleanObject.id) return this.editedBorderStyle;;
    return this.suggestedBorderStyle;
  },
  getBorderStyle(dirtyValue){
    if (!this.dirtyRecording) return  {};   
    if (dirtyValue !== '') return this.editedBorderStyle;
    return {};
  },
  onlyNumber ($event) {
    console.log($event.keyCode); //keyCodes value
    let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
    if (keyCode < 48 || keyCode > 57)  { 
      $event.preventDefault();
    }
  },
  getCommentCleanCompare(){
    if (this.dirtyRecording.comment !== this.cleanRecording.comment ) return this.editTextUnderline;    
    return {}
  }
}