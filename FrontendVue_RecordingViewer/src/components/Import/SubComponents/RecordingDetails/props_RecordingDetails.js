export default {
  dirtyRecording: Object,
  occasionId: Number,
  isStudioRecording: Boolean,
  counter: Number
}