export default {
  editTextUnderline() {return { "text-decoration": "underline", "text-decoration-color": "green" } },   
  editedBorderStyle() { return { border: '1px solid green'}},
  suggestedBorderStyle() { return { border: '1px solid blue'}},
  songList() {
    return this.$store.state.main.songList;
  },
  mp3Url() {
    return "http://localhost:64675/api/import/" + this.dirtyRecording.id;
    //return "http://hipmack.co/wp-content/uploads/2018/02/Kendrick_Lamar_Ft_Travis_Scott_-_Big_Shot.mp3";
  }  
}