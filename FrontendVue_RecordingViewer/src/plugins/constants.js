// This exports the plugin object.
export default {
  // The install method will be called with the Vue constructor as
  // the first argument, along with possible options
  install(Vue) {
    Vue.prototype.$ImportOverViewPage = 1;
    Vue.prototype.$ImportDetailsPage = 2;
    Vue.prototype.$ImportDone = 3;

    Vue.prototype.$isPianist = function(personRoleId) {
      return (personRoleId & 1) == 1;
    };

    Vue.prototype.$isCoach = function(personRoleId) {
      return (personRoleId & 2) == 2;
    };

    Vue.prototype.$isComposer = function(personRoleId) {
      return (personRoleId & 4) == 4;
    };

    Vue.prototype.$isLyricsAuthor = function(personRoleId) {
      return (personRoleId & 8) == 8;
    };
  }
};
