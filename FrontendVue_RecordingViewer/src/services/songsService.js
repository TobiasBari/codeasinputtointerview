import axios from "axios";

const apiClient = axios.create({
  baseURL: "http://localhost:64675/api/",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

export default {
  getSongListContainer() {
    return apiClient.get("/songList");
  }
};
