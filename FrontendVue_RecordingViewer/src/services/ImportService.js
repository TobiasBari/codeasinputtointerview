import axios from "axios";

const apiClient = axios.create({
  baseURL: "http://localhost:64675/api/",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

export default {
  getImportOverviewInfo() {
    return apiClient.get("/Import");
  },
  getImportDetailsInfo() {
    return apiClient.get("/Import/details");
  },
  getMp3Stream(songId) {
    return apiClient.get("/Import/" + songId);
  },
  postImportDetailedInformation(payload, store){
    axios.post("http://localhost:64675/api/Import/", payload)
    .then(response => {
      store.dispatch("setImportStep3Done");
    })
    .catch(e => {
      alert(e);
    })  
  }


};

