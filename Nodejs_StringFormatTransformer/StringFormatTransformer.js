﻿const ResultFormatter = require("./StringFormatTransformer/ResultFormatter");
const BarsOnlyAnalyzer = require("./StringFormatTransformer/Analyzers/BarsOnlyAnalyzers");
const BarsWithCommentAnalyzer = require("./StringFormatTransformer/Analyzers/BarsWithCommentAnalyzer");

module.exports = class StringFormatTransformer {
  constructor(splitter, resultFormatter) {
    this._resultFormatter = resultFormatter
      ? resultFormatter
      : new ResultFormatter(splitter);
    this._splitter = splitter;
  }

  execute(input) {
    if (!input || !input.trim()) return null;

    function analyzerFactory() {
      let analyzer = new BarsOnlyAnalyzer();

      if (!input.includes(":")) return analyzer;
      BarsWithCommentAnalyzer.prototype = analyzer;
      return new BarsWithCommentAnalyzer();
    }

    const analyzer = analyzerFactory(input);
    const segmentList = analyzer.parseToSegmentInfoList(
      input,
      this._splitter.LineBreak
    );
    if (segmentList.length === 1) return this._resultFormatter.getResult(segmentList);
    analyzer.tryAddStartBarOneWhenMissing();

    return this._resultFormatter.getResult(segmentList);
  }
};
