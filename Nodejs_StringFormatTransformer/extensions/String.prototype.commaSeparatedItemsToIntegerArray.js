String.prototype.commaSeparatedItemsToIntegerArray = function() {
    const result = [];
    this.split(',').forEach((startBar) => {
        const temp = startBar.trim();
        if (!isNaN(temp)) result.push(parseInt(temp));
    });
    return result;
};