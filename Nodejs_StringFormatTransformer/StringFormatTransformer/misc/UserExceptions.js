const UserException = require("../Infrastructure/UserException");

class StartBarNumberMissing extends UserException {
    constructor(rowIndex) {
      super(
        "The start bar number must exist before the colon. At row number: " +
          (rowIndex + 1)
      );
    }
  }
  
class CommentIncludesColon extends UserException {
    constructor(rowIndex) {
        super("A comment cannot include a colon." + (rowIndex + 1));
    }
}
  
class StartBarsNotInSequentialOrder extends UserException {
    constructor(rowIndex) {
        super("Start bar numbers must added in sequential ordered number for each line." + (rowIndex + 1));
    }
}

module.exports = {
    StartBarNumberMissing: StartBarNumberMissing,
    CommentIncludesColon:  CommentIncludesColon,
    StartBarsNotInSequentialOrder: StartBarsNotInSequentialOrder
};
