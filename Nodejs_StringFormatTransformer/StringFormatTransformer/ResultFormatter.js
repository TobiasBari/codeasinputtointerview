module.exports = function ResultFormatter(splitter) {
  const _s = splitter;  

this._segmentToString = (segment) => {
    const endBar = segment.endBar !== undefined ? segment.endBar : "";    
    return segment.startBar + _s.Bar + endBar + _s.Space + _s.CommentToTheRight + segment.comment + _s.LineBreak;

}

this._lastSegmentToString = (barnumber) =>  
    `${barnumber}${_s.Bar}${_s.Space}${_s.CommentToTheRight}`

  this.getResult = function (segmentList) {
    if (!segmentList || segmentList.length === 0) return '';
    if (segmentList.length === 1) return this._lastSegmentToString(segmentList[0].startBar);

    let result = "";
    segmentList.forEach((s) => result += this._segmentToString(s));

    const lastSegment = segmentList[segmentList.length - 1];
    if (lastSegment.endBar === undefined) return result;
    result += this._lastSegmentToString(lastSegment.endBar); //NOTE: the potential range problems is taken care of by the commentParser
    return result;
  };
}

