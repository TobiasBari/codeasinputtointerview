require("../../extensions/String.prototype.commaSeparatedItemsToIntegerArray");

module.exports = function BarsOnlyAnalyzer() {
  this._segmentList = [];

  let _createSegment = function(start, end, comment) {
    const segment = {
      startBar: start,
      end: end
    };
    segment.comment = comment === undefined ? "" : comment;
  };

  this.parseToSegmentInfoList = function(input) {
    if (input === null && input.length === 0) return this._segmentList;
    const startBarList = input.commaSeparatedItemsToIntegerArray();
    if (startBarList.length === 0) return this._segmentList;
    if (startBarList.length === 1)
      return this._segmentList.push(_createSegment(startBarList[0]));

    startBarList.sort((a, b) => a - b); //Most likely, bars are already in sorted orders by the user, but just in case.

    for (var i = 0; i < startBarList.length - 1; i++) {
      this._segmentList.push({
        startBar: startBarList[i],
        endBar: startBarList[i + 1],
        comment: ""
      });
    }

    return this._segmentList;
  };

  this.tryAddStartBarOneWhenMissing = function() {
    const segmentList = this._segmentList;
    if (segmentList[0].startBar !== 1) {
      segmentList.splice(0, 0, {
        startBar: 1,
        endBar: segmentList[0].startBar,
        comment: ""
      });
    }
  };
};
