const Exceptions = require("../misc/UserExceptions");

module.exports = function BarsWithCommentAnalyzer() {
  this.parseToSegmentInfoList = function(input, splitBy) {
    if (!this._segmentList)
      throw "BarsWithCommentAnalyzer must inherit from an Analyzer having the _segmentList field created.";
    const lines = input.split(splitBy);
    let previousSegment = null;
    for (let ix = 0; ix < lines.length; ix++) {
      const segment = this._addSegment(lines[ix], ix, previousSegment);
      previousSegment = segment;
    }
    // NOTE: this parser won't sort the segments since the correlating comment should be sequential lost likely according to the lyrics.
    // TODO: So if startBars are not correct, it is deemed to be a user error which will be caught and reported instead.
    return this._segmentList;
  };

  this._addSegment = function(line, ix, previousSegment) {
    const segment = {};
    const lineSplit = line.split(":");
    if (lineSplit.length > 2) throw new Exceptions.CommentIncludesColon(); //TODO: allow for escaping colon in text.
    const unknown = lineSplit[0].trim();
    const barNumber = isNaN(unknown) ? null : parseInt(unknown);
    if (barNumber) {
      //NOTE: bar number is not allowed to be 0.
      segment.startBar = barNumber;
      segment.comment = "";
      if (previousSegment) {
        if (segment.startBar <= previousSegment.startBar) {
          throw new Exceptions.StartBarsNotInSequentialOrder();
        }
        previousSegment.endBar = barNumber;
      }
    }
    if (lineSplit.length === 1) {
      if (!barNumber && previousSegment) {
        previousSegment.comment += Segment.Split.LineBreak + unknown;
      } else throw new Exceptions.StartBarNumberMissing(ix);
    } else {
      if (!barNumber) throw new Exceptions.StartBarNumberMissing(ix);
      segment.comment = lineSplit[1].trim();
    }
    this._segmentList.push(segment);
    return segment;
  };
};
