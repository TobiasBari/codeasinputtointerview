module.exports = class UserException {
  constructor(message) {
    this.name = "User Exception";

    this.message = message;
  }

  toString() {
    return `${this.name}: ${this.message}`;
  }
};
