const ResultFormatter = require("../ResultFormatter.js");
const splitter = require("./splitter.js");

//function getSegment(start, end, comment){ return {startBar: start, endBar : end, comment : comment }}
const getSegment = (start, end, comment) => {
  return { startBar: start, endBar: end, comment: comment };
};
function runTest(setup) {
  const formatter = new ResultFormatter(splitter);
  const actual = formatter.getResult(setup.input);
  expect(actual).toEqual(setup.expected);
}

describe("ResultFormatter.js", () => {
  it("Standard simple usage", () => {
    runTest({
      input: [getSegment(1, 5, "comment 1"), getSegment(5, 10, "comment 2")],
      expected: "1-5 ->comment 1\n5-10 ->comment 2\n10- ->" //NOTE: this module does not know the end bar of the recording. Thus the trailing "10-" which the next segment parser can deal with.
    });
  });

  it("Omitting the start bar 1 should be fine", () => {
    runTest({
      input: [getSegment(5, 10, "comment 1"), getSegment(10, 20, "comment 2")],
      expected: "5-10 ->comment 1\n10-20 ->comment 2\n20- ->"
    });
  });

  it("Segments do _not_ have to be aligned.", () => {
    runTest({
      input: [getSegment(5, 10, "comment 1"), getSegment(20, 30, "comment 2")],
      expected: "5-10 ->comment 1\n20-30 ->comment 2\n30- ->"
    });
  });
});
