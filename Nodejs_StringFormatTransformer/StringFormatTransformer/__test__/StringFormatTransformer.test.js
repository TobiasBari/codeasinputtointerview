const StringFormatTransformer = require("../../StringFormatTransformer.js");
const splitter = require("./splitter.js");
const Exceptions = require("../misc/UserExceptions");

describe("StringFormatTransformer.js - overview of full functionality", () => {
  function runTest(setup) {
    const transformer = new StringFormatTransformer(splitter);
    const actual = transformer.execute(setup.input);
    expect(actual).toEqual(setup.expected);
  }

  it("Test case 1: empty input.", () => {
    runTest({ input: null, expected: null });
    runTest({ input: "", expected: null });
    runTest({ input: "   ", expected: null });
  });

  it("Test input case 2: comma separated string of start bars.", () => {
    runTest({
      input: "1, 5, 8, 15",
      expected: "1-5 ->\n5-8 ->\n8-15 ->\n15- ->"
    });
  });

  it("Test input case 3: first start bar with comment.", () => {
    runTest({
      input: "1: a comment here\n5:another comment here\n8: a third comment",
      expected:
        "1-5 ->a comment here\n5-8 ->another comment here\n8- ->a third comment\n"
    });
  });
});

function runTest(setup) {
  let resultFormatter = { getResult: jest.fn(list => "not used") };
  const transformer = new StringFormatTransformer(splitter, resultFormatter);
  let result = transformer.execute(setup.input);
  if (result !== null) {
    const getResultMock = resultFormatter.getResult.mock;
    const actual = getResultMock.calls[0][0];
    expect(getResultMock.calls.length).toBe(1);
    expect(actual).toEqual(setup.expected);
  } else {
    expect(setup.expected).toEqual(null);
  }
}

let _ = (start, end, comment) => {
  return { startBar: start, endBar: end, comment: comment ? comment : "" };
};

describe("StringFormatTransformer.js - input: comma separated string of start bars - detailed tests", () => {
  it("Test case 1: normal scenario.", () => {
    runTest({
      input: "1, 5, 8, 15",
      expected: [_(1, 5), _(5, 8), _(8, 15)]
    });
  });

  it("Test case 2: bar 1 missing.", () => {
    //NOTE domain info: we know that songs always at bar 1 so this transformer simply adds it if it is missing.
    runTest({
      input: "5, 8, 15",
      expected: [_(1, 5), _(5, 8), _(8, 15)]
    });
  });

  it("Test case 3: wrong order.", () => {
    runTest({
      input: "8, 5, 15",
      expected: [_(1, 5), _(5, 8), _(8, 15)]
    });
  });
});

describe("StringFormatTransformer.js - input: start bars with comment - edge case testing", () => {
  it("Test case 1: normal scenario.", () => {
    runTest({
      input: "1: aa bb\n5:cc dd \n8: ee",
      expected: [_(1, 5, "aa bb"), _(5, 8, "cc dd"), _(8, undefined, "ee")]
    });
  });

  it("Test case 2: first start bar with comment.", () => {
    runTest({
      input: "1:a\n5:b\n8:c",
      expected: [_(1, 5, "a"), _(5, 8, "b"), _(8, undefined, "c")]
    });
  });
});

describe("StringFormatTransformer.js - input: start bars with comment - user errors", () => {
    function getTestExectute(setup) {
        let resultFormatter = { getResult: list => "not used" };
        const transformer = new StringFormatTransformer(splitter, resultFormatter);
        return function execute() {
            transformer.execute(setup.input);
        }
    }

    function runTest(setup) {
        const execute = getTestExectute(setup)        
        if (!setup.errorMessage) execute();    
        else expect(execute).toThrow(setup.errorMessage);
    }   


  it("Test case 1: bar number missing on second row.", () => {
    const testInput = "1:a\nc:b\n8:c\n15: d";
    const errorMessage =
      "The start bar number must exist before the colon. At row number: 2";

    try {
      runTest({
        input: testInput
      });
    } catch (error) {
      expect(error.toString()).toBe(`User Exception: ${errorMessage}`);
    }

    //same thing, but using the Exception class instead for asserting error type.
    expect(getTestExectute({input: testInput}))
        .toThrowError(Exceptions.StartBarNumberMissing)

  });

  it("Test case 2: bar number missing on first row.", () => {
    runTest({
      input: "a\n5:b\n8:c\n15: d",
      errorMessage:
        "The start bar number must exist before the colon. At row number: 1"
    });
  });

  it("Test case 3: A comment cannot include a colon.", () => {
    expect(getTestExectute({input: "1:a\n5:b:b\n8:c\n15: d"}))
    .toThrowError(Exceptions.CommentIncludesColon)
  });

  //TODO: There are more error scenarios to cover, but this will do for an intermediate solution for the prototype.
});
